#include <U8g2lib.h>

#define DBG(X) {}
//#define DBG(X) X

    #ifdef __arm__
    // should use uinstd.h to define sbrk but Due causes a conflict
    extern "C" char* sbrk(int incr);
    #else  // __ARM__
    extern char *__brkval;
    #endif  // __arm__
     
    int freeMemory() {
      char top;
    #ifdef __arm__
      return &top - reinterpret_cast<char*>(sbrk(0));
    #elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
      return &top - __brkval;
    #else  // __arm__
      return __brkval ? &top - __brkval : &top - __malloc_heap_start;
    #endif  // __arm__
    }

// nano pins
//http://www.lehelmatyus.com/wp-content/uploads/2016/12/arduino-nano-pins.png
// oled.setCursor takes x and y in pixels
#define NEXT_PIN 2 //D2
#define PREV_PIN 3 //D3

//#define SDA_PIN A4
//#define SCL_PIN A5


// U8G2 buffered takes too much SRAM. Need to use unbuffered
// https://github.com/olikraus/u8g2/wiki/u8g2setupcpp#ssd1306-128x64_noname
//U8G2_SSD1306_128X64_NONAME_F_HW_I2C oled(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);
U8G2_SSD1306_128X64_NONAME_1_HW_I2C  oled(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);


// strings are too expensive here (eat SRAM)
const int stringLength = 20;
struct Node
{
  Node* next;
  Node* prev;
  char id[stringLength];
  char name[stringLength];
};


Node* currentEffect = nullptr;
Node* lastEffect = nullptr;


char id[stringLength] = "";
char name[stringLength] = "";

// for testing
char defaultID[] = "a";
char defaultName[] = "b";

char *currentEffectID = defaultID;
char *currentEffectName = defaultName;
char currentLHelp[stringLength] = "c";
char currentRHelp[stringLength] = "d";
char currentCHelp[stringLength] = "e";
char currentSamplingRate[stringLength] = "f";
char currentEffectParameter[stringLength] = "g";

void strcopy(char* dst, const char* src)
{
  strncpy(dst, src, stringLength);
  dst[stringLength - 1] = '\0';
}



Node* findEffect(const char* id)
{
  DBG(Serial.print(F("findEffect -- Searching for: "));)
  DBG(Serial.println(id);)

  if (lastEffect == nullptr)
  {
    DBG(Serial.println(F("findEffect -- Empty list"));)
    return nullptr;
  }

  Node* tmp = lastEffect;
  do
  {
    DBG(Serial.print(F("findEffect -- Inspecting: "));)
    DBG(Serial.println(tmp->id);)
    if (strncmp(tmp->id, id, stringLength) == 0)
    {
      // found a match
      DBG(Serial.println(F("findEffect -- Effect found"));)
      return tmp;
    }
    

  } while ((tmp = tmp->next) != lastEffect); // go around the circle at most once
  
  DBG(Serial.println(F("findEffect -- Effect not found"));)
  return nullptr; // not found
}


void appendEffect(char id[], char name[])
{
  // check for duplicates unless this is the first
  if (lastEffect != nullptr && findEffect(id) != nullptr)
  {
    DBG(Serial.println(F("Duplicate effect. Ignoring."));)
    return;
  }


  Node* append = new Node;
  Node* old = lastEffect;
  
  strcopy(append->name, name);
  strcopy(append->id, id);

  // set new "end" of circle
  lastEffect = append;

  if (old == nullptr)
  {
    // ensure circular structure
    DBG(Serial.println(F("appendEffect -- Adding first effect"));)
    append->next = append; 
    append->prev = append;
  }
  else
  { 
    DBG(Serial.println(F("appendEffect -- Adding new effect"));)
    // insert append between previous last node and first node
    append->next = old->next;   
    append->prev = old;
    old->next->prev = append;
    old->next = append;
    
  }
}

/**
 *
 * @param  {int} len : length (px) of string to display
 * @return {uint8_t}     : cursor position for displaying a string in the center (x axis)
 */
uint8_t center(uint8_t len)
{
  return oled.getWidth()/2 - len/2;
}
/**
 * 
 * @return {uint8_t}  : cursor position for displaying a string in the left  (x axis)
 */
uint8_t left()
{
  return 0;
}

/**
 *
 * @param  {uint8_t} len : length (px) of string to display
 * @return {uint8_t}     : cursor position for displaying a string in the right (x axis)
 */
uint8_t right(uint8_t len)
{
  return oled.getWidth() - len;
}

/**
 * 
 * @return {uint8_t}  : cursor position for displaying a string on the bottom (y axis)
 */
uint8_t bottom()
{
  return oled.getHeight() ;
}
/**
 * 
 * @return {uint8_t}  : cursor position for displaying a string on the top (y axis)
 */
uint8_t top()
{
  return oled.getMaxCharHeight();
}

/**
 * 
 * @return {int}  : cursor position for displaying a string on the middle (y axis)
 */
uint8_t middle()
{
  return oled.getHeight() / 2;
}


/**
 * reset display and font settings
 */
void clearDisplay() {
  //oled.clearBuffer(); // clear local buffer. Only used in fully buffered mode, which we are not using due to limited SRAM
  oled.firstPage(); // clear display in paged-mode
  oled.setFont(u8g2_font_unifont_te);
}


void setupPins()
{
  pinMode(NEXT_PIN, INPUT_PULLUP);
  pinMode(PREV_PIN, INPUT_PULLUP);  
  
  delay(100);
}


void setup() {

 // put your setup code here, to run once:
 oled.begin();
 clearDisplay();
 setupPins();
 Serial.begin(9600);
}



void displayEffectName(const char* text)
{
  uint8_t len = oled.getStrWidth(text);
  
  oled.drawStr(center(len),middle(),text);	  
}

void displayLeftControl(const char* hint)
{
  oled.drawStr(left(), top(), hint ); 
}

void displayRightControl(const char *hint)
{
  uint8_t len = oled.getStrWidth(hint);
  
  oled.drawStr(right(len), top(), hint);
}
  
void displayToggleControl(const char* hint)
{

  uint8_t len = oled.getStrWidth(hint);
  
  oled.drawStr(center(len), bottom(), hint );
}

void  displayParameter(const char* val)
{

  oled.drawStr(left(), bottom() - oled.getMaxCharHeight(), val); 
}

void displaySamplingRate(const char* val)
{

  uint8_t len = oled.getStrWidth(val);
  
  oled.drawStr(right(len), bottom() - oled.getMaxCharHeight(), val);   
}

/**
 * Handle add effect command
 * @param  {const char*} effect : effect description <id>:<name>
 */
void addEffect(const char* effect)
{
  char* input = strdup(effect);

  char* pos;
  char* token = strtok_r(input, ":", &pos);

  if (token == NULL)
  {
    free(input);
    return; // invalid
  }

  char id[stringLength];
  strcopy(id, token);

  // find next token
  token = strtok_r(NULL, ":", &pos);
  if (token == NULL)
  {
    free(input);
    return; // invalid
  }

  char name[stringLength];
  strcopy(name, token);

  free(input);

  DBG(Serial.print(F("Name "));)
  DBG(Serial.println(name);)

  DBG(Serial.print(F("ID "));)
  DBG(Serial.println(id);)
  DBG(Serial.flush();)

  // TODO: implement another status for toggle A-B to display on the companion
  // change the comms protocol accordingly. Key $:T and $:E for toggle and effect parameters

  // add a class
  // don't care about delete 
  appendEffect(id, name);
  
}

void startEffect(const char* id)
{
  Node* e = findEffect(id);
  if (e == nullptr)
    return; // invalid

  currentEffectID = e->id;
  currentEffectName = e->name;
  currentEffect = e;
  
  DBG(Serial.println(currentEffectID);)
  DBG(Serial.println(currentEffectName);)

  // clear other display settings
  currentEffectParameter[0] = '\0';
  currentLHelp[0] = '\0';
  currentRHelp[0] = '\0';
  currentCHelp[0] = '\0';
  currentSamplingRate[0] = '\0';
}

void updateSamplingRate(const char* param)
{
  strcopy(currentSamplingRate, param);
  DBG(Serial.println(currentSamplingRate);)
}


void updateEffectParamter(const char* param)
{
  strcopy(currentEffectParameter, param);
  DBG(Serial.println(currentEffectParameter);)
}
/**
 * Process effect hints for the user interface
 * @param  {const char*} param : Format "<item>:<hint>"
 */
void showHelp(const char* param)
{
  char* input = strdup(param);
  char* pos;
  char* token = strtok_r(input, ":", &pos);

  if (token == NULL)
  {
    free(input);
    return; // invalid
  }

  char c = token[0];

  // find next token
  token = strtok_r(NULL, ":", &pos);
  if (token == NULL)
  {
    free(input);
    return; // invalid
  }

  char hint[stringLength];
  strcopy(hint, token);

  // tmp copy not needed any longer
  free(input);

  switch (c)
  {
  case 'L':
    strcopy(currentLHelp, hint);
    DBG(Serial.println(currentLHelp);)
    DBG(Serial.flush();)
    break;
  case 'R':
    strcopy(currentRHelp, hint);
    DBG(Serial.println(currentRHelp);)
    DBG(Serial.flush();)
    break;
  case 'C':
    strcopy(currentCHelp, hint);
    DBG(Serial.println(currentCHelp);)
    DBG(Serial.flush();)
    break;
  

  default:
    break;
  }

}

void processCommand()
{
  if (Serial.available())
  {

    // this is important: read only until \n
    String s = Serial.readStringUntil('\n');
    
    const char* line = s.c_str();
    DBG(Serial.println(line));
    if (strlen(line) <= 2) //invalid command
      return;


    


    DBG(Serial.print(F("free memory: "));)
    DBG(Serial.println(freeMemory());)
    DBG(Serial.print(F("line: "));)
    DBG(Serial.println(line);)

    
    char cmd = line[0];
    const char* param = line + 2;

    DBG(Serial.println(param); )
    switch (cmd)
    {
    case '#': // effect information
      DBG(Serial.print(F("new effect: "));)
      DBG(Serial.println(param);)
      DBG(Serial.flush();)
      addEffect(param);
      break;
    case '!': // effect selected
      startEffect(param);
      break;
    case '%': // sampling rate
      updateSamplingRate(param);
      break;
    case '$': // parameter
      updateEffectParamter(param);
      break;
    case '?': // effect help
      showHelp(param);
      break;      
    default:
      break;
    }
  }
}



/**
 * Short delay to debounce the buttons
 */
void debounce()
{
  delay(100);
}

int nextEffect = 0; // 0 = stay, -1 = previous, 1 = next

// PIN1 is connected to LED. Therefore it will not be pulled up correctly
// we need to measure the converse (connect button to V+ and consider it presssed, when HIGH
// PIN5 is connected to reset, we need to set a fuse to be able to use this button
void processControls()
{
  if (digitalRead(NEXT_PIN) == LOW)
  {
    debounce();
    nextEffect = 1;
  }
  else if (digitalRead(PREV_PIN) == LOW)
  {
    debounce();
    nextEffect = -1;
  }
  else
  {
      nextEffect = 0;
  }
  
    
  
}

void changeEffect()
{
  if (nextEffect == 0 || lastEffect == nullptr) // no change or empty effect list
    return;


  Node* cur = currentEffect;
  Node* next;
  if (cur == nullptr)
    cur = lastEffect;

  if (nextEffect == 1) // next
  {
    next = cur->next;
  }
  else if (nextEffect == -1) // back
  {
    next = cur->prev;
  }

  // send request to switch effect
  Serial.print("S:");
  Serial.println(next->id);
  
}

void loop() {
  processControls();
  processCommand();
  changeEffect();

  clearDisplay();
  
  do {
    displayEffectName(currentEffectName);
    displayLeftControl(currentLHelp);
    displayRightControl(currentRHelp);
    displayToggleControl(currentCHelp);
    displayParameter(currentEffectParameter);
    displaySamplingRate(currentSamplingRate);
  } while ( oled.nextPage() );

  //oled.sendBuffer();					// transfer internal memory to the display. only used in fully buffered mode


}